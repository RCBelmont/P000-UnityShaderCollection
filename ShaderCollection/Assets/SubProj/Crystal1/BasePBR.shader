﻿Shader "MyPBR/BasePBR"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" { }
        _MainColor ("Color", Color) = (1, 1, 1, 1)
        _AmbientColor ("_AmbientColor", Color) = (1, 1, 1, 1)
        _HightColor ("_HightColor", Color) = (1, 1, 1, 1)
        _HightColorPow ("_HightColorPow", float) = 2
        _AmbientCube ("AmbientCube", Cube) = "white" { }
        _AmbientLOD ("_AmbientLOD", float) = 1
        _AmbientPow ("_AmbientPow", float) = 1
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100
        
        GrabPass { }
        Pass
        {
            Tags { "LightMode" = "ForwardBase" }
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            
            struct appdata
            {
                float4 vertex: POSITION;
                float2 uv: TEXCOORD0;
                float3 noraml: NORMAL;
            };
            
            struct v2f
            {
                float2 uv: TEXCOORD0;
                float4 vertex: SV_POSITION;
                float3 normal: NORMAL;
                float3 worldPos: TEXCOORD1;
                float4 lightScreenPos: TEXCOORD2;
                float4 ScreenPos: TEXCOORD3;
                float depth: TEXCOORD4;
                float lightClipPos: TEXCOORD5;
            };
            
            sampler2D _MainTex;
            float4 _MainTex_ST;
            float3 _HightColor;
            float3 _MainColor;
            float4x4 _MyMatrixVP;
            float4x4 _MyMatrixVPINS;
            float4x4 _MyMatrixV;
            sampler2D _LightDepth;
            sampler2D _GrabTexture;
            sampler2D _CameraDepthTexture;
            float4 _MyZBufferParam;
            float4 _MyLightPos;
            float4 _AmbientColor;
            uniform samplerCUBE _AmbientCube;
            float _AmbientLOD;
            float _AmbientPow;
            
            sampler2D _MyDepth;
            
            float4 MyComputScreenPos(float4 pos)
            {
                
                float4 o = pos * 0.5f;
                o.xy = float2(o.x, o.y * _ProjectionParams.x) + o.w;
                o.zw = pos.zw;
                return o;
            }
            float MyLinearEyeDepth(float z)
            {
                return 1.0 / (_MyZBufferParam.z * z + _MyZBufferParam.w);
            }
            
            inline float3 MYUnityObjectToViewPos(in float3 pos)
            {
                return mul(_MyMatrixV, mul(unity_ObjectToWorld, float4(pos, 1.0))).xyz;
            }
            
            
            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                float4 lightClipPos = mul(_MyMatrixVP, mul(unity_ObjectToWorld, float4(v.vertex.rgb, 1.0)));
                o.lightScreenPos = MyComputScreenPos(lightClipPos);
                o.ScreenPos = MyComputScreenPos(o.vertex);
                o.normal = UnityObjectToWorldNormal(v.noraml);
                
                o.depth = -MYUnityObjectToViewPos(v.vertex).z;
                return o;
            }
            
            half4 frag(v2f i): SV_Target
            {
                fixed3 col = tex2D(_MainTex, i.uv);
                float2 lightUV = i.lightScreenPos.xy / i.lightScreenPos.w;
                float2 SreenUV = i.ScreenPos.xy / i.ScreenPos.w;
                // return half4(lightUV.yyy, 1);
                float lightDepth1 = 1 - tex2D(_LightDepth, lightUV * 0.8 + 0.1).r;
                float depth0 = MyLinearEyeDepth(lightDepth1);
                float4 LightEnterPos = mul(_MyMatrixVPINS, float4(i.lightScreenPos.xy, lightDepth1, 1.0));
                
                float3 deltaVector = i.worldPos - _MyLightPos;
                float3 lightDir = normalize(deltaVector);
                float3 viewDir = -normalize(UnityWorldSpaceViewDir(i.worldPos));
                //return half4(dot(viewDir, -lightDir).xxx, 1);
                //float transDis = i.depth - depth0;
                
                float transDis = length(deltaVector);
                //return half4(lightUV * 0.8 + 0.1, 0, 1);
                //return half4(tex2D(_LightDepth, lightUV * 0.8 + 0.1).rgb, 1);
                //return half4(transDis.xxx, 1);
                float minD = 0.0;
                float PenetrationWeight = (transDis - minD) / (4 - minD);
                
                //PenetrationWeight *= 1 - step(3, transDis);
                PenetrationWeight = 1 - PenetrationWeight;
                //PenetrationWeight *=  dot(viewDir, -lightDir);
                // return half4(PenetrationWeight.xxx, 1);
                fixed ndl = saturate(dot(i.normal, normalize(_MyLightPos)));
                fixed vdl = (dot(viewDir, normalize(_MyLightPos)));
                 half fresnel = pow(1.0 - max(0, dot(-i.normal, viewDir)), 3) * 1;
                float3 ReflectRay = reflect(normalize(-i.normal), viewDir);
                float4 ReflectColor = texCUBElod(_AmbientCube, float4(ReflectRay, _AmbientLOD));
                float3 finalCol = saturate(PenetrationWeight) * _HightColor * _MainColor;
                //return half4(fresnel.xxx, 1);
                return half4(finalCol + (1 - saturate(PenetrationWeight)) * _AmbientColor + ReflectColor * saturate(PenetrationWeight) * _AmbientPow + fresnel, 1);
            }
            
            
            ENDCG
            
        }
    }
    FallBack "Diffuse"
}
