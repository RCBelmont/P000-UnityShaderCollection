﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Rendering;
[ExecuteInEditMode]
public class Test : MonoBehaviour
{
    public GameObject SubCamera;
    public RenderTexture _depthRt;
    public GameObject Target;
    public Material _mat;
    private CommandBuffer _cmd;
    private Camera _c;
    private Renderer _renderer;

    
    // Start is called before the first frame update
    void Start()
    {
        _c = SubCamera.GetComponent<Camera>();
        _cmd = new CommandBuffer();
        _depthRt = new RenderTexture(512, 512, 16, RenderTextureFormat.Depth);
        _renderer = Target.GetComponent<Renderer>();
        Shader.SetGlobalTexture("_LightDepth", _depthRt);
    }

    void OnDestroy()
    {
        _depthRt.Release();
        _cmd.Clear();
        Destroy(_depthRt);
    }

    // Update is called once per frame
    void Update()
    {
        Matrix4x4 v = _c.worldToCameraMatrix;
        Profiler.BeginSample("Test_SetCmdBuffer");
        Profiler.BeginSample("Test_CalcMatrix");
        Matrix4x4 vp = GL.GetGPUProjectionMatrix(_c.projectionMatrix, true) * _c.worldToCameraMatrix;
        Profiler.EndSample();
        Profiler.BeginSample("Test_BuildCmd");
        _cmd.Clear();
        _cmd.name = "RRRRRR";
        _cmd.SetRenderTarget(_depthRt);
        _cmd.ClearRenderTarget(true, true, new Color(1,1,1,1));
        _cmd.SetGlobalMatrix("_MyMatrixVP", vp);
        _cmd.SetGlobalMatrix("_MyMatrixV", v);
        _cmd.SetGlobalMatrix("_MyMatrixVPINS", vp.inverse);
        _cmd.SetGlobalVector("_MyLightPos", _c.transform.position);
        _cmd.SetGlobalVector("_MyProjectionParams",
            new Vector4(1, _c.nearClipPlane, _c.farClipPlane, 1 / _c.farClipPlane));
        _cmd.SetGlobalVector("_MyZBufferParam",
            new Vector4(1 - _c.farClipPlane / _c.nearClipPlane, _c.farClipPlane / _c.nearClipPlane,
                1 / _c.farClipPlane - 1 / _c.nearClipPlane, 1 / _c.nearClipPlane));
        _cmd.DrawRenderer(_renderer, _mat);
        Profiler.EndSample();
        Profiler.BeginSample("Test_ExecuteCmd");
        Graphics.ExecuteCommandBuffer(_cmd);
        Profiler.EndSample();
        Profiler.EndSample();
        //_cmd.DrawRenderer();
    }
}