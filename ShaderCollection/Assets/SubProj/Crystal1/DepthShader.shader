﻿Shader "Unlit/DepthShader"
{
    Properties { }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100
       
       
        Pass
        {
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            
            struct appdata
            {
                float4 vertex: POSITION;
                float2 uv: TEXCOORD0;
            };
            
            struct v2f
            {
                float2 uv: TEXCOORD0;
                float4 vertex: SV_POSITION;
                float3 worldPos: TEXCOORD1;
                float depth: TEXCOORD2;
            };
            
            sampler2D _MainTex;
            float4x4 _MyMatrixVP;
            float4x4 _MyMatrixV;
            float3 _MyLightPos;
            float4 _MyZBufferParam;
            inline float3 MYUnityObjectToViewPos(in float3 pos)
            {
                return mul(_MyMatrixV, mul(unity_ObjectToWorld, float4(pos, 1.0))).xyz;
            }
            v2f vert(appdata v)
            {
                v2f o;
                o.worldPos = mul(unity_ObjectToWorld, float4(v.vertex.rgb, 1.0));
                o.vertex = mul(_MyMatrixVP, mul(unity_ObjectToWorld, float4(v.vertex.rgb, 1.0)));
                o.uv = v.uv;
                o.depth = -MYUnityObjectToViewPos(v.vertex).z;
                return o;
            }
            
            fixed4 frag(v2f i): SV_Target
            {
                
                float z = (1 / i.depth - _MyZBufferParam.w) / (_MyZBufferParam.z);
                
                return half4( z.xxx,1); 
            }
            ENDCG
            
        }
    }
}
