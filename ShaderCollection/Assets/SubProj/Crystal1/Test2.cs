﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Test2 : MonoBehaviour
{
    public GameObject target;

    public int num = 500;

    private RenderTexture _rt;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Camera>().depthTextureMode = DepthTextureMode.Depth;
        CommandBuffer cmd = new CommandBuffer();
        _rt = new RenderTexture(750, 1334, 16, RenderTextureFormat.Depth);
        _rt.name = "!!!!!TEX";
        //Shader.SetGlobalTexture("_MyDepth", _rt);
        cmd.GetTemporaryRT(Shader.PropertyToID("_MyDepth"), 750, 1334, 16);
        cmd.Blit(Shader.GetGlobalTexture("_CameraDepthTexture"), Shader.PropertyToID("_MyDepth"));
        this.GetComponent<Camera>().AddCommandBuffer(CameraEvent.AfterDepthTexture, cmd);
    }

    void OnDestroy()
    {
        _rt.Release();
        Destroy(_rt);
    }
    // Update is called once per frame
    void Update()
    {
    }
}