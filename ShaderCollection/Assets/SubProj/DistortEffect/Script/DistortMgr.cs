﻿//*****************************************************************************
//Created By ZJ on 2019年1月9日.
//
//@Description 扭曲效果管理器
//*****************************************************************************
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using UnityEngine;

namespace JZYX.Effect.Distort
{
    public class DistortMgr
    {
        //单例
        private static DistortMgr _instance;
        public static DistortMgr GetInstance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DistortMgr();
                }

                return _instance;
            }
        }

        //游戏相机缓存字典
        public Dictionary<Camera, DistortCamera> _camCmdDic = new Dictionary<Camera, DistortCamera>();
        //分辨率降低倍数
        public const int RESOLUTION_DIV = 2;


#if UNITY_EDITOR
        //场景相机缓存字典
        public Dictionary<Camera, DistortCamera> _SceneCmdDic = new Dictionary<Camera, DistortCamera>();
#endif

       

      
        /// <summary>
        /// 注册相机
        /// </summary>
        /// <param name="c"></param>
        /// <param name="dc"></param>
        public void RegisterCamera(Camera c, DistortCamera dc)
        {
            int oldCount = _camCmdDic.Count;
            if (!_camCmdDic.ContainsKey(c) && c.cameraType == CameraType.Game)
            {
                _camCmdDic.Add(c, dc);
            }
#if UNITY_EDITOR
            if (oldCount <= 0 && _camCmdDic.Count > 0)
            {
                Camera[] sceneCameraL = SceneView.GetAllSceneCameras();
                if (sceneCameraL.Length > 0 && !_SceneCmdDic.ContainsKey(sceneCameraL[0]))
                {
                    _SceneCmdDic.Add(sceneCameraL[0], sceneCameraL[0].gameObject.AddComponent<DistortCamera>());
                }
            }
#endif
        }
        /// <summary>
        /// 注销相机
        /// </summary>
        /// <param name="c"></param>
        public void UnregisterCamera(Camera c)
        {
            if (_camCmdDic.ContainsKey(c))
            {
                _camCmdDic.Remove(c);
#if UNITY_EDITOR
                if (_camCmdDic.Count <= 0)
                {
                    _SceneCmdDic.Clear();
                    ;
                }
#endif
            }
        }
        /// <summary>
        /// 添加绘制指令
        /// </summary>
        /// <param name="c"></param>
        /// <param name="r"></param>
        public void AddDistorDrawCmd(Camera c, Renderer r)
        {
            if (_camCmdDic.ContainsKey(c) && c.cameraType == CameraType.Game)
            {

                _camCmdDic[c].AddDistortDraw(r);
            }
#if UNITY_EDITOR
            if (_SceneCmdDic.ContainsKey(c) && c.cameraType == CameraType.SceneView)
            {
                _SceneCmdDic[c].AddDistortDraw(r);
            }
#endif
        }
    }
}
