﻿using UnityEngine;
using UnityEngine.Rendering;

namespace JZYX.Effect.Distort
{
    [ExecuteInEditMode]
    public class DistortObj : MonoBehaviour
    {
        

        private CommandBuffer _cmdBuffer;
        private Renderer _renderer;

        // Start is called before the first frame update
        void Start()
        {
            _renderer = this.GetComponent<Renderer>();

        }

        // Update is called once per frame
        void Update()
        {
        }
        /// <summary>
        /// 向绘制到该物体的相机添加绘制指令, 并关闭renderer
        /// 相机Culling之后会调用要绘制的GameObject的OnWillRenderObject函数
        /// 但最后是否绘制是根据Render的开启情况决定的
        /// </summary>
        public void OnWillRenderObject()
        {
            DistortMgr.GetInstance.AddDistorDrawCmd(Camera.current, _renderer);
            _renderer.enabled = false;
        }
        /// <summary>
        /// 绘制完成后开启Render方便下次Culling调用
        /// </summary>
        public void OnRenderObject()
        {
            _renderer.enabled = true;
        }
    }
}