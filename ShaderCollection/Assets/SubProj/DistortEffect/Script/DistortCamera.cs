﻿//*****************************************************************************
//Created By ZJ on 2019年1月9日.
//
//@Description 扭曲效果相机组件
//*****************************************************************************
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Rendering;

namespace JZYX.Effect.Distort
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Camera))]
    public class DistortCamera : MonoBehaviour
    {
        private int _grabTexTID = Shader.PropertyToID("_DistortTextureT");
        private int _grabTexID = Shader.PropertyToID("_DistortTexture");
        private CommandBuffer _grabCmd;//抓图CmdBuffer
        private CommandBuffer _drawCmd;//绘制CmdBuffer
        private int _drawCout = 0;//绘制数量
        private bool _grabOn = false;//是否启动抓图
        private Camera _camera;

        void OnEnable()
        {
            //CmdBuffer
            _camera = this.GetComponent<Camera>();
            _grabCmd = new CommandBuffer();
            _grabCmd.name = "DistortGrabTex";
            _camera.AddCommandBuffer(CameraEvent.AfterForwardAlpha, _grabCmd);
            _drawCmd = new CommandBuffer();
            _drawCmd.name = "Darw DistorObj";
            _camera.AddCommandBuffer(CameraEvent.BeforeImageEffects, _drawCmd);
            //将相机注册到扭曲管理器中
            if (_camera.cameraType == CameraType.Game)
            {
                DistortMgr.GetInstance.RegisterCamera(_camera, this);
            }
        }

        void OnDisable()
        {
            //注销相机并移除CmdBuffer
            if (_camera.cameraType == CameraType.Game)
            {
                DistortMgr.GetInstance.UnregisterCamera(_camera);
            }

            _camera.RemoveCommandBuffer(CameraEvent.BeforeImageEffects, _drawCmd);
            if (_grabOn)
            {
                _camera.RemoveCommandBuffer(CameraEvent.AfterForwardAlpha, _grabCmd);
                _grabOn = false;
            }
        }

        /// <summary>
        /// 添加一个扭曲效果物件的绘制
        /// </summary>
        /// <param name="r"></param>
        public void AddDistortDraw(Renderer r)
        {
            if (r != null)
            {
                _drawCout++;

                _drawCmd.DrawRenderer(r, r.sharedMaterial);
            }
        }
        //Culling前清空CmdBuffer
        private void OnPreCull()
        {
            _drawCmd.Clear();
            _grabCmd.Clear();
        }
        //渲染前根据是否渲染物体更新CmdBuffer
        private void OnPreRender()
        {
            if (_drawCout > 0)
            {
                Profiler.BeginSample("DS_UpdateCmd");
                UpdateCmdBuffer(_grabCmd);
                Profiler.EndSample();
                _grabOn = true;
            }

        }
        //渲染结束后重置渲染格式
        private void OnPostRender()
        {
            if (_drawCout > 0)
            {
                _drawCout = 0;
            }
        }
        //更新cmdBuffer内容, 便于在相机属性发生改变是能即时变化
        public CommandBuffer UpdateCmdBuffer(CommandBuffer cmd)
        {

            int div = DistortMgr.RESOLUTION_DIV;
            cmd.GetTemporaryRT(_grabTexTID, _camera.pixelWidth, _camera.pixelHeight, 0);
            cmd.GetTemporaryRT(_grabTexID, _camera.pixelWidth / div, _camera.pixelHeight / div, 0);
            cmd.Blit(BuiltinRenderTextureType.CameraTarget, _grabTexTID);
            cmd.Blit(_grabTexTID, _grabTexID);
            cmd.ReleaseTemporaryRT(_grabTexTID);
           
            return cmd;
        }
    }
}