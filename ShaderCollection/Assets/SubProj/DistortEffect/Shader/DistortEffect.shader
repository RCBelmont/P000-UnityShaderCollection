﻿Shader "Unlit/DistortEffect"
{
    Properties
    {
        _BumpTex ("Texture", 2D) = "white" { }
        
        _BumpParam ("BumpParam", Vector) = (0, 0, 1, 0)
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100
        GrabPass
        {
            "_myGrabTexture"
        }
        Pass
        {
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            
            
            #include "UnityCG.cginc"
            
            struct appdata
            {
                float4 vertex: POSITION;
                float2 uv: TEXCOORD0;
            };
            
            struct v2f
            {
                float2 uv: TEXCOORD0;
                float4 vertex: SV_POSITION;
                half2 screenUV: TEXCOORD1;
            };
            
            
            sampler2D _DistortTexture;
            
            sampler2D _myGrabTexture;
            sampler2D _BumpTex;
            float4 _BumpTex_ST;
            
            
            float4 _BumpParam;
            
            
            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _BumpTex);
                float4 screenPos = ComputeScreenPos(o.vertex);
                o.screenUV = screenPos.xy / (screenPos.w);
                #if UNITY_UV_STARTS_AT_TOP
                    o.screenUV.y = step(0, _ProjectionParams.x) + o.screenUV.y * - _ProjectionParams.x ;
                #endif
                return o;
            }
            
            fixed4 frag(v2f i): SV_Target
            {
                // sample the texture
                
                half3 distortF = tex2D(_BumpTex, i.uv + _BumpParam.xy * _Time.y);
                distortF = (distortF - 0.5) * 2;
                half2 uv = i.screenUV + distortF.rg * _BumpParam.z / 20;
                fixed4 col = tex2D(_DistortTexture, uv);
                return col;
            }
            ENDCG
            
        }
    }
}
