﻿//UI扫光
//Description:
//By zj 2019/1/10
Shader "GUI/DefaultWithFlush"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" { }
        _Color ("Tint", Color) = (1, 1, 1, 1)
        
        _StencilComp ("Stencil Comparison", Float) = 8
        _Stencil ("Stencil ID", Float) = 0
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255
        _Rotate ("Rotate", Range(0, 360)) = 45
       
        _Width ("宽度", Float) = 0.8
        _Smooth ("平滑过渡", Range(0, 6)) = 0.9
        _ColorTint ("颜色偏移", Color) = (1,1,1,1)
        _Bright ("亮度", Float) = 0.6
        _FlushTime("扫动时间0.1s", int) = 20
        _IntervalTime("间隔时间0.1s", int) = 20
        
        _ColorMask ("Color Mask", Float) = 15
        
        [Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
    }
    
    SubShader
    {
        Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "PreviewType" = "Plane" "CanUseSpriteAtlas" = "True" }
        
        Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass [_StencilOp]
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }
        
        Cull Off
        Lighting Off
        ZWrite Off
        ZTest [unity_GUIZTestMode]
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask [_ColorMask]
        
        Pass
        {
            Name "Default"
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0
            
            #include "UnityCG.cginc"
            #include "UnityUI.cginc"
            
            #pragma multi_compile __ UNITY_UI_CLIP_RECT
            #pragma multi_compile __ UNITY_UI_ALPHACLIP
            
            struct appdata_t
            {
                float4 vertex: POSITION;
                float4 color: COLOR;
                float2 texcoord: TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };
            
            struct v2f
            {
                float4 vertex: SV_POSITION;
                fixed4 color: COLOR;
                float2 texcoord: TEXCOORD0;
                float4 worldPosition: TEXCOORD1;
                UNITY_VERTEX_OUTPUT_STEREO
            };
            
            sampler2D _MainTex;
            fixed4 _Color;
            fixed4 _TextureSampleAdd;
            float4 _ClipRect;
            float4 _MainTex_ST;
            float _Rotate;
            half _Pos;
            half _Width;
            half _Smooth;
            fixed3 _ColorTint;
            half _Bright;
            half _FlushTime;
            half _IntervalTime;
            
            v2f vert(appdata_t v)
            {
                v2f OUT;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
                OUT.worldPosition = v.vertex;
                OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);
                
                OUT.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
                
                OUT.color = v.color * _Color;
                return OUT;
            }
            
            fixed4 frag(v2f IN): SV_Target
            {
                half4 color = (tex2D(_MainTex, IN.texcoord) + _TextureSampleAdd) * IN.color;
                half2 uv = (IN.texcoord - 0.5) * 2;
                float theta = _Rotate * 0.0174;
                float2x2 rota = {
                    float2(cos(theta), -sin(theta)), float2(sin(theta), cos(theta))
                };
                half2 oPos = mul(rota, uv);
                half pos = saturate(_Time.y * 10 % (_FlushTime + _IntervalTime) / _FlushTime);
                half Location = lerp(-1/abs(cos(theta)), 1/abs(cos(theta)), pos);
                float dis = abs(oPos.x - Location);
                #ifdef UNITY_UI_CLIP_RECT
                    color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
                
                #ifdef UNITY_UI_ALPHACLIP
                    clip(color.a - 0.001);
                #endif
                //return half4(IN.texcoord.x, 0, 0, 1);
                half lightPow = (1 - saturate(step(_Width, dis)));
                half dd = saturate((_Width - dis) / _Width);
                half D = smoothstep(0, _Smooth, dd);
                return half4(color.rgb + (D) * _Bright * _ColorTint * sin(pos * 3.14), color.a);
            }
            ENDCG
            
        }
    }
}
