﻿Shader "Unlit/2DWave"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" { }
      
        _Power ("Power", Range(0, 5)) = 0
        _Pos1 ("Pos1", Vector) = (0, 0, 0, 0)
        _Pos2 ("Pos2", Vector) = (0, 0, 0, 0)
		_Speed1("Speed1", Float) = 1
		_Speed2("Speed2", Float) = 1
		_Freq1("Freq1", Float) = 1
		_Freq2("Freq2", Float) = 1
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100
        Pass
        {
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            
            #include "UnityCG.cginc"
            
            struct appdata
            {
                float4 vertex: POSITION;
                float2 uv: TEXCOORD0;
            };
            
            struct v2f
            {
                float2 uv: TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex: SV_POSITION;
              
            };
            
            sampler2D _MainTex;
			float4 _MainTex_ST;
            float2 _Pos1;
            float2 _Pos2;
            float _Speed1;
            float _Speed2;
            float _Freq1;
            float _Freq2;
           
            float _Power;
            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o, o.vertex);
			
                return o;
            }
            
            fixed4 frag(v2f i): SV_Target
            {
                // sample the texture
                float2 dv = i.uv- _Pos1;
                float2 dv1 =  i.uv - _Pos2;
                
                float dis = length(dv);
                float dis1 = length(dv1);
                
             
				float speed1 = _Time.y * _Speed1;
				float speed2 = _Time.y * _Speed2;
                float r1 = (sin(dis1 * 3.14 *_Freq1 + (speed1)) + 1) / 2;
                float g1 = (cos(dis1 * 3.14 *_Freq1 + (speed1)) + 1) / 2;
                float b1 = 1;
                
				half3 col2 = UnpackNormal(half4(r1, g1, b1, 1));


                float r = (sin(dis * 3.14 * _Freq2  + speed2) + 1) / 2;
                float g = (cos(dis * 3.14 * _Freq2 + speed2) + 1) / 2;
                float b = 1;

               
               
                
               
                half3 col1 = UnpackNormal(half4(r, g, b, 1));
                fixed4 col = tex2D(_MainTex, i.uv  + (col1.rg + col2.rg * 0) * _Power / 10 );

				float2 dir = (col1.rg + col2.rg) * _Power / 10;
				//Debug
				//return half4(col1,1);
				//return half4(col2,1);
                return col * 0.6 + (dir.x) * 2;
            }
            ENDCG
            
        }
    }
}
