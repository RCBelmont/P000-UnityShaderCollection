﻿Shader "Unlit/Crystal2Trans"
{
    Properties
    {
        _MainTex ("主纹理", 2D) = "white" { }
        _MainColor ("基础颜色", Color) = (1, 1, 1, 1)
        _NoiseTex ("噪声纹理", 2D) = "white" { }
        _TexColor ("噪声颜色", Color) = (1, 1, 1, 1)
        _Ambient ("环境光颜色", Color) = (1, 1, 1, 1)
        _RefColor ("高光颜色", Color) = (1, 1, 1, 1)
        _FresnelCol ("菲涅尔颜色", Color) = (1, 1, 1, 1)
        _MyLightPos ("透射光源位置", Vector) = (1, 1, 1, 1)
        _TransCol ("透射光源颜色", Color) = (1, 1, 1, 1)
        _TansDis ("透射距离", float) = 1
        _AmbientCube ("环境贴图", Cube) = "white" { }
        _RefLOD ("环境贴图LOD", float) = 1
        _RefStr ("环境贴图强度", float) = 1
        _BumpParam ("折射强度", float) = 1
    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" "LightMode" = "ForwardBase" "Queue" = "Transparent" }
        LOD 100
        ZWrite Off
        //Blend SrcAlpha OneMinusSrcAlpha
        GrabPass {"_BackgroundTexture" }
        Pass
        {
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fwdbase
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            
            struct appdata
            {
                float4 vertex: POSITION;
                float2 uv: TEXCOORD0;
                float3 normal: NORMAL;
            };
            
            struct v2f
            {
                float2 uv: TEXCOORD0;
                float4 vertex: SV_POSITION;
                float3 normal: NORMAL;
                float3 viewDir: TEXCOORD1;
                float3 lightDir: TEXCOORD2;
                float3 localPos: TEXCOORD3;
                float2 uv1: TEXCOORD4;
                float2 screenUV: TEXCOORD5;
            };
            
            sampler2D _NoiseTex;
            float4 _NoiseTex_ST;
            sampler2D _MainTex;
            float4 _MainTex_ST;
           
            sampler2D _BackgroundTexture;
            float4 _MyLightPos;
            
            
            half4 _MainColor;
            half4 _Ambient;
            half4 _RefColor;
            half4 _FresnelCol;
            half4 _TexColor;
            half3 _TransCol;
          
            half _TansDis;
            half _RefLOD;
            half _RefStr;
            half _BumpParam;
            uniform samplerCUBE _AmbientCube;
            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _NoiseTex);
                o.uv1 = TRANSFORM_TEX(v.uv, _MainTex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                o.viewDir = normalize(WorldSpaceViewDir(v.vertex));
                o.lightDir = normalize(WorldSpaceLightDir(v.vertex));
                o.localPos = v.vertex;
                
                float4 screenPos = ComputeScreenPos(o.vertex);
                o.screenUV = screenPos.xy / (screenPos.w);
                #if UNITY_UV_STARTS_AT_TOP
                    o.screenUV.y = step(0, _ProjectionParams.x) + o.screenUV.y * - _ProjectionParams.x ;
                #endif
                
                return o;
            }
            
            fixed4 frag(v2f i): SV_Target
            {
                // sample the texture
                i.normal = normalize(i.normal);
                i.viewDir = normalize(i.viewDir);
                //i.lightDir = i.viewDir;
                fixed3 ref1 = (reflect(-i.viewDir, i.normal));
                half2 uv = i.uv + ref1 / 3;
                
                float3 deltaVector = i.localPos - _MyLightPos;
                float transDis = length(deltaVector);
                float minD = 0.0;
                float PenetrationWeight = (transDis) / (_TansDis);
                PenetrationWeight = clamp(1 - PenetrationWeight, 0.2, 1);
                fixed4 mianCol = tex2D(_MainTex, i.uv1) * _MainColor;
                fixed3 col = tex2D(_NoiseTex, uv) * _TexColor + mianCol;
                fixed diffTerm = saturate(dot(i.normal, i.lightDir));
                fixed3 finalCol = col * (diffTerm + _Ambient);
                fixed3 ref = normalize(reflect(i.lightDir, i.normal));
                half refStr = saturate(dot(ref, -i.viewDir));
                half fresnel = pow(1.0 - max(0, dot(i.normal, i.viewDir)), 3) * 1.5;
                // /return half4(fresnel.xxx, 1);
                float4 ReflectColor = texCUBElod(_AmbientCube, float4(ref1, _RefLOD));
                
                finalCol = finalCol + saturate(PenetrationWeight) * _TransCol ;
                
               
                i.screenUV = i.screenUV + ref1.rg * _BumpParam / 20;
                fixed4 screenCol = tex2D(_BackgroundTexture, i.screenUV);
                finalCol = finalCol + pow(refStr * 1, 4) * _RefColor + fresnel * _FresnelCol + ReflectColor * _RefStr;
                finalCol = lerp(screenCol, finalCol, mianCol.a);
                return fixed4(finalCol, 1);
            }
            ENDCG
            
        }
    }
}
