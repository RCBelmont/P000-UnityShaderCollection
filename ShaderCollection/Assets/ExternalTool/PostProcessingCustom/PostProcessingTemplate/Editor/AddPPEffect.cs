﻿//*****************************************************************************
//Created By ZJ on 2018/3/28.
//
//@Description 自动添加PostProcessing特效组件
//*****************************************************************************
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class AddPPEffect : Editor
{
   
    [MenuItem("GameObject/Effects/AddPPEffect")]
    public static void AddObjAndCom()
    {
        GameObject select = Selection.activeGameObject;
        Camera mianC = select.GetComponent<Camera>();
        if (mianC == null)
        {
            Debug.LogError("请添加在主相机上");
            return;
        }
        PostProcessLayer ppL = mianC.gameObject.GetComponent<PostProcessLayer>();
        if (ppL == null)
        {
            ppL = mianC.gameObject.AddComponent<PostProcessLayer>();
            ppL.volumeLayer = LayerMask.GetMask(new string[]{"PostProcessing"});
            ppL.volumeTrigger = mianC.transform;
        }

        GameObject ppV = GameObject.Find("Post-process Volume");
        if (ppV == null)
        {
            ppV = new GameObject();
            ppV.AddComponent<BoxCollider>();
            ppV.AddComponent<PostProcessVolume>().isGlobal = true;
            ppV.name = "Post-process Volume";
            ppV.layer = LayerMask.NameToLayer("PostProcessing");
        }
    }

   
}