﻿Shader "Unlit/Cloud"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" { }
        _Noise ("Noise", 2D) = "white" { }
        _ColorLight ("_ColorLight", Color) = (1, 1, 1, 1)
        _ColorDark ("_ColorDark", Color) = (0, 0, 0, 0)
        _Fade ("_Fade", Float) =2
       
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" "Queue" = "Transparent" }
        LOD 100
        // /ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
      
        Pass
        {
            
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            //#pragma multi_compile_fwd
            
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            
            struct appdata
            {
                float4 vertex: POSITION;
                float2 uv: TEXCOORD0;
                float3 normal: NORMAL;
                float4 tanDir:TANGENT;
            };
            
            struct v2f
            {
                float2 uv: TEXCOORD0;
                float4 color: COLOR;
                float4 vertex: SV_POSITION;
                float3 worldPos: TEXCOORD1;
                float3 lightDir: TEXCOORD2;
                float3 normal: NORMAL;
                float depth: TEXCOORD3;
                float vuv: TEXCOORD4;
                float2 screenUV: TEXCOORD5;
                float3 tanDir: TEXCOORD6;
                float3 biTanDir: TEXCOORD7;
                
            };
            
            sampler2D _MainTex;
            sampler2D _Noise;
            sampler2D _CameraDepthTexture;
            sampler2D _NormalTex;
            float4 _MainTex_ST;
            float4 _ColorLight;
            float4 _ColorDark;
            float _Fade;
            
            
            v2f vert(appdata v)
            {
                v2f o;
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.vuv = v.uv;
                fixed4 ncol = tex2Dlod(_Noise, half4(o.uv + half2(_Time.y *0, _Time.y * 1), 0, 0));
                o.color = ncol;
                v.vertex += half4(v.normal * ncol.r * 0.15, 0);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                o.lightDir = UnityWorldSpaceLightDir(o.worldPos);
                o.normal = UnityObjectToWorldNormal(v.normal);
                COMPUTE_EYEDEPTH(o.depth);
                 float4 screenPos = ComputeScreenPos(o.vertex);
                o.screenUV = screenPos.xy / screenPos.w;
                o.tanDir =mul(unity_ObjectToWorld, v.tanDir.rgb);
                o.biTanDir = normalize(cross(o.normal, o.tanDir) * v.tanDir.w);
                return o;
            }
            
            fixed4 frag(v2f i): SV_Target
            {
                // sample the texture
                //fixed4 col = tex2D(_MainTex, i.uv);
                half3 ncol = UnpackNormal(tex2D(_NormalTex, i.uv + half2(_Time.y * 0.0, _Time.y * 0)));
                
                i.normal = normalize(i.normal);

                half3 normal =normalize(i.biTanDir * ncol.r + i.tanDir * ncol.g + i.normal *ncol.b);

                half ndl = saturate(dot(i.normal, i.lightDir));
                fixed4 col = lerp(_ColorDark, _ColorLight, ndl);
                float oDepth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.screenUV));
                float deltaDepth = (oDepth - i.depth);
                half3 finalCol = col.rgb + UNITY_LIGHTMODEL_AMBIENT.rgb * 0;
                // /return half4(deltaDepth.xxx, 1);
                //return half4(length(_WorldSpaceLightPos0-i.worldPos).xxx, 1);
                return half4(finalCol, saturate(deltaDepth*_Fade));
                //return i.color;
            }
            ENDCG
            
        }
    }
    //Fallback "Diffuse"
}
