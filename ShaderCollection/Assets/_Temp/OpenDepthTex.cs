﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class OpenDepthTex : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnEnable() {
        
        this.GetComponent<Camera>().depthTextureMode = DepthTextureMode.Depth;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
