//*****************************************************************************
//Created By ZJ on 2018年6月12日.
//
//@Description 水面材质管理器
//*****************************************************************************

using UnityEngine;
using UnityEngine.Rendering;


[ExecuteInEditMode]
public class WaterEffect : MonoBehaviour
{
    [Header("强制快速模式")] public bool ForceFastMode = false;
    [Header("水面基础透明度(在快速模式下生效)")] public float WaterAlpha = 0.6f;

    private RenderTexture _screenTexture; //保存屏幕图像的RenderTexture
    private GameObject _biCameraObj; //副相机GameObject
    private Camera _biCamera; //副相机Camera组件
    private Camera _cam; //当前相机

    private CommandBuffer _cmd; //CommandBuffer
    private const int Div = 5; //renderTexture分辨率衰减系数

    private int _waterAlphaID = Shader.PropertyToID("_WaterAlpha"); //记录Shader属性ID
    void OnEnable()
    {
        Start();
    }

    void OnDisable()
    {
        Shader.DisableKeyword("WATER_HIGH");
        _cam.depthTextureMode = DepthTextureMode.None;
        _screenTexture.DiscardContents();
    }

    // Use this for initialization
    void Start()
    {
        _cam = this.GetComponent<Camera>();
        //创建一个低分辨率的RenderTexture用于接收屏幕画面
        if (_screenTexture == null)
        {
            _screenTexture =
                RenderTexture.GetTemporary(Screen.width / Div, Screen.height / Div, 0, RenderTextureFormat.Default);
        }
        //为相机添加CommandBuffer，在天空盒渲染完成后将当前渲染的画面拷贝到一个低分辨率的RenderTexture中
        //好处：1.避免使用GrabPass 2.可以控制纹理分辨率，降低采样成本
        if (_cmd != null)
        {
            //移除CommandBuffer
            if (_cmd != null)
            {
                _cam.RemoveCommandBuffer(CameraEvent.AfterSkybox, _cmd);
                _cmd.Release();
                _cmd = null;
            }
        }
        if (_cmd == null)
        {
            _cmd = new CommandBuffer();
            _cmd.name = "WATER_EFFECT";
            _cmd.GetTemporaryRT(Shader.PropertyToID("_DownSampleScreen"), new RenderTextureDescriptor(_cam.pixelWidth, _cam.pixelHeight));
            _cmd.GetTemporaryRT(Shader.PropertyToID("_DownSampleScreen2"), new RenderTextureDescriptor(_cam.pixelWidth / Div, _cam.pixelHeight / Div));
            _cmd.Blit(BuiltinRenderTextureType.CurrentActive, Shader.PropertyToID("_DownSampleScreen"));
            _cmd.Blit(BuiltinRenderTextureType.CameraTarget, Shader.PropertyToID("_DownSampleScreen2"));
            _cmd.SetGlobalTexture("_WaterScreenTex", Shader.PropertyToID("_DownSampleScreen"));
            _cam.AddCommandBuffer(CameraEvent.AfterSkybox, _cmd);
        }
    }

    // Update is called once per frame
    void OnPreRender()
    {
        
        int QualityLv = QualitySettings.GetQualityLevel();
        _screenTexture.DiscardContents();
        if (QualityLv >= 1 && !ForceFastMode)
        {
            _cam.depthTextureMode = DepthTextureMode.Depth;
            if (!Shader.IsKeywordEnabled("WATER_HIGH"))
            {
                Shader.EnableKeyword("WATER_HIGH");
            }
        }
        else
        {
            _cam.depthTextureMode = DepthTextureMode.None;
            Shader.SetGlobalFloat(_waterAlphaID, WaterAlpha);
            if (Shader.IsKeywordEnabled("WATER_HIGH"))
            {
                Shader.DisableKeyword("WATER_HIGH");
            }
        }


        ;
    }

    void OnDestroy()
    {
        Shader.DisableKeyword("WATER_HIGH");
        _cam.depthTextureMode = DepthTextureMode.None;
        //移除CommandBuffer
        if (_cmd != null)
        {
            _cam.RemoveCommandBuffer(CameraEvent.AfterSkybox, _cmd);
            _cmd.Release();
            _cmd = null;
        }
        //释放RenderTexture
        if (_screenTexture != null)
        {
            RenderTexture.ReleaseTemporary(_screenTexture);
            _screenTexture = null;
        }
    }
}