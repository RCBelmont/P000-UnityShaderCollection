﻿//水面材质，适用于湖面，水池或海面
//Description:
//By zj 2018/6/12
Shader "JZYX/Effect/Water"
{
	Properties
	{
		_MainColor("基础颜色", Color) = (1,1,1,1)
		_BumpMap ("法线1", 2D) = "bump" {}
		_NormalLOD("法线1细节等级", Range(1, 5)) = 1
		_BumpMap1 ("法线2", 2D) = "bump" {}
		_Norma2LOD("法线2细节等级", Range(1, 5)) = 1
		_Specular("反射度(平衡漫反射和反射)", Range(0, 1)) = 1
		_Gloss("光滑度", Range(0, 1)) = 0.5
		_DirSpecularStrenth("直接反射强度", Range(0,5)) = 1
		_SpecularStrenth("间接反射强度", Range(0,5)) = 1
		_IndirectColorStrength("间接光强度", Range(0, 2)) = 1
		_FresnelPow("菲涅尔强度", Range(0, 10)) = 1
		
		_DeepWaterColor("深部颜色", Color) = (0,0,0,0)
		_DeepColorGrad("深度变化梯度", float) = 1
		_RefractStrenth("折射强度", Range(0,2)) = 0
		_FadeOutDistance("消隐距离", float) = 200
		_FadeOutSpeed("消隐衰减", float) = 20
		_OffsetSpeed("法线偏移速度(XYZW分别对应两张法线图的xy)", Vector) =(0,0,0,0)


		
		

	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue" = "Transparent-1" }
		LOD 100

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
		 Tags {
                "LightMode"="ForwardBase"
            }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fwdbase
			#pragma multi_compile _ WATER_HIGH
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"
			#include "UnityStandardBRDF.cginc"
			#pragma target 3.0

			sampler2D _BumpMap;
			float4 _BumpMap_ST;
			sampler2D _BumpMap1;
			float4 _BumpMap1_ST;
			float _Gloss;
			float4 _MainColor;
			float _Specular;
			float _SpecularStrenth;
			float _IndirectColorStrength;
			float _FresnelPow;
			int _NormalLOD;
			int _Norma2LOD;
			sampler2D _CameraDepthTexture; 
			sampler2D _WaterDepthTex; 
			sampler2D _WaterScreenTex;
			float4 _DeepWaterColor;
			half _RefractStrenth;
			half _DeepColorGrad;
			half _WaterAlpha;
			half _FadeOutDistance;
			half _FadeOutSpeed;
			half _DirSpecularStrenth;
			float4 _OffsetSpeed;
		
			UnityLight MainLight(){
				UnityLight l;
				l.color = _LightColor0.rgb;
				l.dir = _WorldSpaceLightPos0.xyz;
				return l;
			}
			inline float DecodeDepth(float4 value)
			{	
				return value.x + value.y / float(256);
			}
			inline UnityGI GIFunction(UnityLight light, float3 worldPos, float3 viewDir, float atten, float3 normal){
				UnityGIInput d;
				d.light = light;
                d.worldPos = worldPos;//i.posWorld.xyz;
                d.worldViewDir = viewDir;//viewDirection;
                d.atten = atten;//attenuation;
				d.ambient = 0;
				d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
				#if defined(UNITY_SPECCUBE_BLENDING) || defined(UNITY_SPECCUBE_BOX_PROJECTION)
					d.boxMin[0] = unity_SpecCube0_BoxMin; // .w holds lerp value for blending
				#endif
					#ifdef UNITY_SPECCUBE_BOX_PROJECTION
					d.boxMax[0] = unity_SpecCube0_BoxMax;
					d.probePosition[0] = unity_SpecCube0_ProbePosition;
					d.boxMax[1] = unity_SpecCube1_BoxMax;
					d.boxMin[1] = unity_SpecCube1_BoxMin;
					d.probePosition[1] = unity_SpecCube1_ProbePosition;
				#endif
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - _Gloss;
                ugls_en_data.reflUVW = reflect(-viewDir, normal);
                return UnityGlobalIllumination(d, 1, normal, ugls_en_data );
			}


			struct appdata
			{
				float4 vertex : POSITION;
				half3 normal:NORMAL;
				float2 uv0 : TEXCOORD0;
				float4 tangent :TANGENT;
			
			};

			struct v2f
			{
				float2 uv0 : TEXCOORD1;
				float4 vertex : SV_POSITION;
				float3 viewDir:TEXCOORD2;
				float4 worldPos:TEXCOORD3;
				float3 normal:NORMAL; 
				UNITY_SHADOW_COORDS(4)
				float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
				float2 uv1 : TEXCOORD7;
				float2 uv2 : TEXCOORD8;
				float4 screenPos : TEXCOORD9;
				float depth : TEXCOORD10;
   
			};

			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv0 = v.uv0;
				o.uv1 = TRANSFORM_TEX(v.uv0, _BumpMap);
				o.uv2 = TRANSFORM_TEX(v.uv0, _BumpMap1);
				o.viewDir = normalize(WorldSpaceViewDir(v.vertex));
				o.worldPos = mul(unity_ObjectToWorld, v.vertex); 
				o.normal = normalize(UnityObjectToWorldNormal(v.normal));
				o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normal, o.tangentDir) * v.tangent.w);
				o.screenPos = ComputeScreenPos(o.vertex);
				COMPUTE_EYEDEPTH(o.depth);
				return o;
			}
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				//half4 col = tex2D(_MainTex, i.uv +  half2(1, 1));
				half3 diffuseColor  = _MainColor;

				half roughness = 1 - _Gloss;
				half roughness2 = roughness * roughness;

				float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normal);
				
			    float3 localNormal0 = UnpackNormal(tex2Dlod(_BumpMap, half4(i.uv1 + _Time.x * _OffsetSpeed.xy, 0, _NormalLOD))).rgb; 
				float3 localNormal1 = UnpackNormal(tex2Dlod(_BumpMap1, half4(i.uv2 + _Time.x * _OffsetSpeed.zw, 0, _Norma2LOD))).rgb; 
				
				float3 localNormal = localNormal0 + localNormal1 ;
				float3 normal = normalize(mul(localNormal, tangentTransform));
				
				//GlobalIllumination 
				//InitLight
				UNITY_LIGHT_ATTENUATION(atten, i, i.worldPos);
				UnityLight light = MainLight();
				UnityGI gi = GIFunction(light, i.worldPos, i.viewDir, atten,  normal);
				float3 halfVector = normalize(i.viewDir+gi.light.dir);
				float ndl = max(0, dot(normal, gi.light.dir));
				float ndv = max(0, dot(normal, i.viewDir));
				float ndh = max(0, dot(normal, halfVector));
				float ldh = max(0, dot(gi.light.dir, halfVector));
				////Specular
				float3 specularColor = float3(_Specular, _Specular, _Specular);
				float specularMonochrome;
				diffuseColor = EnergyConservationBetweenDiffuseAndSpecular(diffuseColor, specularColor, specularMonochrome);
				//float3 directSpecular = 0.8 * gi.light.color * pow(ndh, 256);
				 float visTerm = SmithJointGGXVisibilityTerm( ndl, ndv, roughness2);
                float normTerm = GGXTerm(ndh, roughness2);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
				 #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * ndl);
				half surfaceReduction;
				surfaceReduction = 1.0/(roughness*roughness + 1.0);
				specularPBL *= any(specularColor) ? 1.0 : 0.0;
				float3 directSpecular = gi.light.color*specularPBL*FresnelTerm(specularColor, ldh) * _DirSpecularStrenth;
				
				
				//float3 directSpecular = 0.8 * gi.light.color * pow(ndh, 256 * _Specular);
				float3 indirectSpecular = (gi.indirect.specular);
				float fresnel = pow(clamp(1.0 - ndv, 0, 1), _FresnelPow);
                float3 specular = (directSpecular + indirectSpecular *  fresnel) ;
				half3 finalColor = diffuseColor * ndl *gi.light.color + gi.indirect.diffuse  * _IndirectColorStrength;
				float3 refract =  float3(0,0,0);
				float2 screenUV = (i.screenPos.xy/i.screenPos.w);
				float2 screenUVD = (i.screenPos.xy/i.screenPos.w) + (localNormal.xy * _RefractStrenth / 20);
				float alpha = min(1, _WaterAlpha);

				#ifdef WATER_HIGH
				float d = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, screenUV));
				float d2 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, screenUVD));
				half2 screenDepth = half2(d, d2);
				half2 waterDepth = half2 (i.depth, i.depth); 
				half2 deltaDepth = screenDepth - waterDepth;
				float flag = clamp(step(0, deltaDepth.x) * step(0, deltaDepth.y), 0, 1);
				float2 refractUV = flag > 0 ?screenUVD : screenUV;	
				refractUV.y = 1-refractUV.y;
				refract = tex2D(_WaterScreenTex, refractUV).rgb;	
				refract = lerp(refract, _DeepWaterColor, clamp((flag > 0 ?deltaDepth.y : deltaDepth.x) / max(_DeepColorGrad, 1), 0, 1));

				alpha = lerp(0, 1, clamp(deltaDepth.x / 1, 0, 1));
				if (deltaDepth.y < 0.3 ){
					return half4(1,1,1, saturate( 0.7 - alpha*2));
				}
				#endif

				
				float3 col = finalColor + specular * max(0,_SpecularStrenth) + (1-fresnel)*refract;
				half disToCam = distance(i.worldPos, _WorldSpaceCameraPos);
				half distanceAlpha = 1 -  clamp((disToCam - _FadeOutDistance)/ _FadeOutSpeed, 0, 1);
				//return fixed4((fixed3)distanceAlpha, 1);
				return fixed4(col, min(alpha, distanceAlpha));
			}

			
			ENDCG
		}
	}
}
