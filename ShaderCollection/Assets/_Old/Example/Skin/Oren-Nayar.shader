﻿Shader "Unlit/Oren-Nayar"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_MainColor("MainColor", Color) = (1,1,1,1)
		_NormalMap("NormalMap", 2D) = "bump" {}
		_Roughness("Roughness", Range(0,100)) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque"}
		LOD 100

		Pass
		{
		Tags  {
			"LightMode" = "ForwardBase"
		}
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
		
			
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			#include "../../CGLib/CGUtil.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal: NORMAL;
				float4 tangent: TANGENT;
			};

			struct v2f
			{
				float4 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
				LIGHTING_COORDS(1,2)
				float3 viewDir : TEXCOORD3;
				float3 posWorld : TEXCOORD4;
				float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
				
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _NormalMap;
			float4 _NormalMap_ST;

			float4 _MainColor;
			float4 _LightColor0;
			float _Roughness;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
				o.uv.zw = TRANSFORM_TEX(v.uv, _NormalMap);
				o.normal =normalize(UnityObjectToWorldNormal(v.normal));
				o.viewDir = normalize(WorldSpaceViewDir(v.vertex));
				o.posWorld = mul(unity_ObjectToWorld, v.vertex);
				o.tangentDir =UnityObjectToWorldDir(v.tangent);
				o.bitangentDir = normalize(cross(o.normal, o.tangentDir) * v.tangent.w);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{

				//// calc normal
				float3x3 tangentTransform =  float3x3(i.tangentDir, i.bitangentDir, i.normal);
				float3 norCol = UnpackNormal(tex2D(_NormalMap, TRANSFORM_TEX(i.uv.zw, _NormalMap)));
				float3 normal = normalize(mul(norCol, tangentTransform));
				//// sample the texture
				fixed4 texCol = SRGBToLinear(tex2D(_MainTex, i.uv.xy));
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float ndl = dot(normal, lightDirection);
				float ndv = dot(normal, i.viewDir);
				float tao = _Roughness;
				float tao2 = (tao * tao);
				float A = 1 - 0.5 * tao2 / (tao2 + 0.33);
				float B = 0.45*tao2/(tao2 + 0.09);
				float angleNL = acos(ndl);
				float angleNV = acos(ndv);
				float alpha = max(angleNL, angleNV);
				float beta = min(angleNL, angleNV);
				float C = max(0, dot(lightDirection, i.viewDir));
				float L = max(ndl, 0) * (A + (B * C * sin(alpha) * tan(beta)));

				float atten = LIGHT_ATTENUATION(i);
				float3 attenLigthColor = atten * SRGBToLinear(_LightColor0.xyz);
				
				half3 direct = texCol.rgb * _MainColor * clamp(L, 0, 1) * attenLigthColor;
				half3 inDirect = texCol.rgb * _MainColor * SRGBToLinear(UNITY_LIGHTMODEL_AMBIENT.rgb);
				
			
				return half4(LinearToSRGB(direct + inDirect), 1); 
			}
			ENDCG
		}
	}
}
