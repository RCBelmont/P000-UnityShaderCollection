﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Hidden/Texture Gaussian Blur" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_BlurSize ("Blur Size", Float) = 1.0
	}
	SubShader {
		CGINCLUDE
		
		#include "UnityCG.cginc"
		
		sampler2D _MainTex;  
		half4 _MainTex_TexelSize;
		float _BlurSize;
		  
		struct v2f {
			float4 pos : SV_POSITION;
			half2 uv[7]: TEXCOORD0;
		};
		  
		v2f vertBlurVertical(appdata_img v) {
			v2f o;
			o.pos = UnityObjectToClipPos(v.vertex);
			
			half2 uv = v.texcoord;

			o.uv[0] = uv + float2(0, -5.5 * _MainTex_TexelSize.y) * _BlurSize;		 
			o.uv[1] = uv + float2(0, -3.5 * _MainTex_TexelSize.y) * _BlurSize;
			o.uv[2] = uv + float2(0, -1.5 * _MainTex_TexelSize.y) * _BlurSize;
			o.uv[3] = uv + float2(0, 0 * _MainTex_TexelSize.y) * _BlurSize;
			o.uv[4] = uv + float2(0, 1.5 * _MainTex_TexelSize.y) * _BlurSize;
			o.uv[5] = uv + float2(0, 3.5 * _MainTex_TexelSize.y) * _BlurSize;
			o.uv[6] = uv + float2(0, 5.5 * _MainTex_TexelSize.y) * _BlurSize;

			return o;
		}
		
		v2f vertBlurHorizontal(appdata_img v) {
			v2f o;
			o.pos = UnityObjectToClipPos(v.vertex);
			
			half2 uv = v.texcoord;

			o.uv[0] = uv + float2(-5.5 * _MainTex_TexelSize.x, 0) * _BlurSize;		 
			o.uv[1] = uv + float2(-3.5 * _MainTex_TexelSize.x, 0) * _BlurSize;
			o.uv[2] = uv + float2(-1.5 * _MainTex_TexelSize.x, 0) * _BlurSize;
			o.uv[3] = uv + float2(0, 0 * _MainTex_TexelSize.x) * _BlurSize;
			o.uv[4] = uv + float2(1.5 * _MainTex_TexelSize.x, 0) * _BlurSize;
			o.uv[5] = uv + float2(3.5 * _MainTex_TexelSize.x, 0) * _BlurSize;
			o.uv[6] = uv + float2(5.5 * _MainTex_TexelSize.x, 0) * _BlurSize;

			return o;
		}

		// v2f vertBlurHorizontal(appdata_img v) {
		// 	v2f o;
		// 	o.pos = UnityObjectToClipPos(v.vertex);
			
		// 	half2 uv = v.texcoord;
			
		// 	o.uv[0] = uv;
		// 	o.uv[1] = uv + float2(_MainTex_TexelSize.x * 1.0, 0.0) * _BlurSize;
		// 	o.uv[2] = uv - float2(_MainTex_TexelSize.x * 1.0, 0.0) * _BlurSize;
		// 	o.uv[3] = uv + float2(_MainTex_TexelSize.x * 2.0, 0.0) * _BlurSize;
		// 	o.uv[4] = uv - float2(_MainTex_TexelSize.x * 2.0, 0.0) * _BlurSize;
					 
		// 	return o;
		// }
		
		fixed4 fragBlur(v2f i) : SV_Target {
			float4 weight[7] = 
			{
				{0.006, 0.0, 0.0, 0.0},
				{0.061, 0.0, 0.0, 0.0},
				{0.242, 0.25, 0.25, 0.0},
				{0.383, 0.5, 0.5, 0.0},
				{0.242, 0.25, 0.20, 0.0},
				{0.061, 0.0, 0.0, 0.0},
				{0.006, 0.0, 0.0, 0.0}
			};
			
			float4 a;
			a = tex2D(_MainTex, i.uv[0]) * weight[0];
			a += tex2D(_MainTex, i.uv[1]) * weight[1];
			a += tex2D(_MainTex, i.uv[2]) * weight[2];
			a += tex2D(_MainTex, i.uv[3]) * weight[3];
			a += tex2D(_MainTex, i.uv[4]) * weight[4];
			a += tex2D(_MainTex, i.uv[5]) * weight[5];
			a += tex2D(_MainTex, i.uv[6]) * weight[6];

			return a;
		}
		    
		ENDCG
		
		ZTest Always Cull Off ZWrite Off
		
		Pass {
			NAME "GAUSSIAN_BLUR_VERTICAL"
			
			CGPROGRAM
			  
			#pragma vertex vertBlurVertical  
			#pragma fragment fragBlur
			  
			ENDCG  
		}

		Pass {
			NAME "GAUSSIAN_BLUR_VERTICAL"
			
			CGPROGRAM
			  
			#pragma vertex vertBlurHorizontal  
			#pragma fragment fragBlur
			  
			ENDCG  
		}
	} 
	FallBack "Diffuse"
}
