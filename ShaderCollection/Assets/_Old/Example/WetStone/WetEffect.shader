﻿Shader "Unlit/WetEffect"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" { }
        _Normal ("Normal", 2D) = "bump" { }
        _Wave ("Wave", 2D) = "bump" { }
        _Smoothness ("Smoothness", Range(0, 1)) = 0.5
        _Mettallic ("Mettallic", Range(0, 1)) = 0.5
        _Wet ("Wet", Range(0, 10)) = 0.5
        _WaterDir ("WaterDir", Vector) = (0, 0, 0, 0)
        _WaterNormal1 ("WaterNormal1", 2D) = "bump" { }
        _WaterNormal2 ("WaterNormal2", 2D) = "bump" { }
       
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100
        
        Pass
        {
            Tags { "LightMode" = "ForwardBase" }
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityStandardUtils.cginc"
            #pragma multi_compile_fwdbase
            struct appdata
            {
                float4 vertex: POSITION;
                float2 uv: TEXCOORD0;
                float3 normal: NORMAL;
                float4 tangent: TANGENT;
            };
            
            struct v2f
            {
                float2 uv: TEXCOORD0;
                float3 viewDir: TEXCOORD1;
                float4 worldPos: TEXCOORD2;
                float3 normal: NORMAL;
                float3 tangentDir: TEXCOORD3;
                float3 bitangentDir: TEXCOORD4;
                float2 uv1: TEXCOORD5;
                float4 vertex: SV_POSITION;
                UNITY_SHADOW_COORDS(6)
                #if UNITY_SHOULD_SAMPLE_SH
                    half3 sh: TEXCOORD7; // SH
                #endif
                float3 localPos: TEXCOORD8;
                float4 waterUv: TEXCOORD9;
            };
            
            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _Normal;
            float4 _Normal_ST;
            float _Smoothness;
            float _Mettallic;
            float _Wet;
            float4 _WaterDir;
            sampler2D _Wave;
            sampler2D _WaterNormal1;
            float4 _WaterNormal1_ST;
            sampler2D _WaterNormal2;
            float4 _WaterNormal2_ST;
            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv1 = TRANSFORM_TEX(v.uv, _Normal);
                o.waterUv.xy = TRANSFORM_TEX(v.uv, _WaterNormal1);
                o.waterUv.zw = TRANSFORM_TEX(v.uv, _WaterNormal2);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                o.viewDir = -normalize((o.worldPos.xyz - _WorldSpaceCameraPos));
                o.tangentDir = normalize(mul(unity_ObjectToWorld, float4(v.tangent.xyz, 0.0))).xyz;
                float3 worldNormal = normalize(UnityObjectToWorldNormal(v.normal));
                o.localPos = v.vertex;
                o.normal = worldNormal;
                o.bitangentDir = normalize(cross(o.normal, o.tangentDir) * v.tangent.w);
                #ifndef LIGHTMAP_ON
                    #if UNITY_SHOULD_SAMPLE_SH && !UNITY_SAMPLE_FULL_SH_PER_PIXEL
                        o.sh = 0;
                        o.sh = ShadeSHPerVertex(worldNormal, o.sh);
                    #endif
                #endif // !LIGHTMAP_ON
                return o;
            }
            
            inline UnityGI GIFunction(UnityLight light, float3 viewDir, float atten, float3 normal, float4 worldPos, half3 sh, float smooth)
            {
                UnityGIInput data;
                data.light = light;
                data.worldPos = worldPos;
                data.worldViewDir = viewDir;
                data.atten = atten;
                data.ambient = sh;
                data.lightmapUV = (half4)0;
                data.probeHDR[0] = unity_SpecCube0_HDR;
                data.probeHDR[1] = unity_SpecCube1_HDR;
                #if defined(UNITY_SPECCUBE_BLENDING) || defined(UNITY_SPECCUBE_BOX_PROJECTION)
                    data.boxMin[0] = unity_SpecCube0_BoxMin; // .w holds lerp value for blending
                #endif
                #ifdef UNITY_SPECCUBE_BOX_PROJECTION
                    data.boxMax[0] = unity_SpecCube0_BoxMax;
                    data.probePosition[0] = unity_SpecCube0_ProbePosition;
                    data.boxMax[1] = unity_SpecCube1_BoxMax;
                    data.boxMin[1] = unity_SpecCube1_BoxMin;
                    data.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                Unity_GlossyEnvironmentData glossData;
                glossData.roughness = 1 - smooth;
                glossData.reflUVW = reflect(-viewDir, normal);
                return UnityGlobalIllumination(data, 1, normal);
            }
            
            fixed4 frag(v2f i): SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                float trueWet = saturate(_Wet / 10 + i.localPos.y) * step(0.1, _Wet);
                float trueSmoothness = saturate(_Smoothness + clamp(trueWet, 0, 0.5));
                
                UnityLight light;
                light.color = _LightColor0.rgb;
                light.dir = normalize(_WorldSpaceLightPos0.xyz);
                
                float3x3 tangentTransform = float3x3(i.tangentDir, i.bitangentDir, i.normal);
                float3 localNormal = UnpackNormal(tex2D(_Normal, i.uv1)).rgb;
                float3 waterNormal1 = UnpackNormal(tex2D(_WaterNormal1, i.waterUv.xy + _WaterDir.xy * _Time.x)).rgb * saturate(trueWet);
                float3 waterNormal2 = UnpackNormal(tex2D(_WaterNormal2, i.waterUv.zw + _WaterDir.zw * _Time.x)).rgb * saturate(trueWet) ;
                float3 wave =  UnpackNormal(tex2D(_Wave, i.uv1)).rgb;

                localNormal = localNormal + waterNormal1 + waterNormal2 + wave;
               
                
                 float3 normal = normalize(mul(localNormal, tangentTransform));
                float ndl = max(0, dot(normal, light.dir));
                float ndv = max(0, dot(normal, i.viewDir));
                float3 halfDir = normalize(i.viewDir + normalize(light.dir));
                
                float ndh = max(0, dot(normal, halfDir));
                float ldh = max(0, dot(light.dir, halfDir));
                
                UNITY_LIGHT_ATTENUATION(atten, i, i.worldPos);
                float3 sh = 0;
                #if UNITY_SHOULD_SAMPLE_SH
                    sh = i.sh;
                #endif
                UnityGI gi = GIFunction(light, i.viewDir, atten, normal, i.worldPos, sh, trueSmoothness);
                half3 finalColor = col.rgb;
                float3 diffuseColor = finalColor * gi.light.color;
                float specularMonochrome;
                float3 specular;
                
                float trueMetallic = saturate(_Mettallic + clamp(trueWet, 0, 0.2));
                diffuseColor = DiffuseAndSpecularFromMetallic(diffuseColor, trueMetallic, specular, specularMonochrome);
                
                
                float perceptualRoughness = SmoothnessToPerceptualRoughness(trueSmoothness);
                float roughness = PerceptualRoughnessToRoughness(perceptualRoughness);
                roughness = max(roughness, 0.002);
                
                half V = SmithJointGGXVisibilityTerm(ndl, ndv, roughness);
                float D = GGXTerm(ndh, roughness);
                
                float specularTerm = V * D * UNITY_PI * ndl;
                
                 half grazingTerm = saturate(_Smoothness + (1-specularMonochrome));
                float3 finalDiffuse = (diffuseColor * ndl + gi.indirect.diffuse * col.rgb) * lerp(1, 0.4, trueWet);
                finalColor = finalDiffuse + specularTerm * light.color * FresnelTerm(specular, ldh) +  gi.indirect.specular * FresnelLerp (specular, grazingTerm, ndv);;
                //finalColor = specularTerm * light.color;
                //return half4(i.viewDir , 1);
                //return half4(half2(abs(distance(half2(i.localPos.x,0), half2(0,0))) * 20, abs(distance(half2(0,i.localPos.y), half2(0,0)) *20)),0,1);
               
                return half4(finalColor, 1);
            }
            ENDCG
            
        }
    }
    Fallback "Standard"
}
