﻿using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess(typeof(FogOfWarRender), PostProcessEvent.AfterStack, "Custom/FogOfWar")]
public sealed class FogOfWar : PostProcessEffectSettings
{
    //EXSAMPLE
    //[Range(0f, 1f), Tooltip("Grayscale effect intensity.")]
    //public FloatParameter blend = new FloatParameter { value = 0.5f };

    public TextureParameter FogMask = new TextureParameter();
}

public sealed class FogOfWarRender : PostProcessEffectRenderer<FogOfWar>
{
    public override void Render(PostProcessRenderContext context)
    {
        //EXSAMPLE
        var sheet = context.propertySheets.Get(Shader.Find("Hidden/Custom/FogOfWarShader"));
        //sheet.properties.SetFloat("_Blend", settings.blend);
        sheet.properties.SetTexture("_FogMask", settings.FogMask);

        
        Matrix4x4 frustumCorners = Matrix4x4.identity;
        float fov = context.camera.fieldOfView;
        float near = context.camera.nearClipPlane;
        float far = context.camera.farClipPlane;
        float aspect = context.camera.aspect;

        float halfHeight = near * Mathf.Tan(fov * 0.5f * Mathf.Deg2Rad);
        Transform cameraTransform = context.camera.gameObject.transform;
        Vector3 toRight = cameraTransform.right * halfHeight * aspect;
        Vector3 toTop = cameraTransform.up * halfHeight;

        Vector3 topLeft = cameraTransform.forward * near + toTop - toRight;
        float scale = topLeft.magnitude / near;

        topLeft.Normalize();
        topLeft *= scale;

        Vector3 topRight = cameraTransform.forward * near + toRight + toTop;
        topRight.Normalize();
        topRight *= scale;


        Vector3 bottomLeft = cameraTransform.forward * near - toTop - toRight;
        bottomLeft.Normalize();
        bottomLeft *= scale;

        Vector3 bottomRight = cameraTransform.forward * near + toRight - toTop;
        bottomRight.Normalize();
        bottomRight *= scale;

        frustumCorners.SetRow(0, topLeft);
        frustumCorners.SetRow(1, bottomLeft);
        frustumCorners.SetRow(2, topRight);
        frustumCorners.SetRow(3, bottomRight);

        sheet.properties.SetMatrix("_FrustumCornersRay", frustumCorners);

        context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
    }
}