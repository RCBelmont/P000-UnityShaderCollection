﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogOfWarMask : MonoBehaviour
{
    public RenderTexture FogMask;
    public Transform Player;

    private Material _m;

    private Vector3 _oldPos;
    // Use this for initialization
    void Start()
    {
        _m = new Material(Shader.Find("Hidden/FogMaskShader"));
        _m.hideFlags = HideFlags.HideAndDontSave;
        _oldPos = new Vector3();
		Graphics.Blit(FogMask, FogMask, _m, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(_oldPos, Player.position) > -1) {
			_m.SetVector("_PlayerPos", Player.position);
			RenderTexture temp = RenderTexture.GetTemporary(FogMask.descriptor);
			Graphics.Blit(FogMask, temp, _m, 1);
			Graphics.Blit(temp, FogMask);
			RenderTexture.ReleaseTemporary(temp);
			_oldPos = Player.position;
		}

    }
}
