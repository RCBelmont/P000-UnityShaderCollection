﻿Shader "Hidden/Custom/FogOfWarShader"
{
    HLSLINCLUDE
    
    #include "PostProcessing/Shaders/StdLib.hlsl"
    
    TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
    TEXTURE2D_SAMPLER2D(_FogMask, sampler_FogMask);
    TEXTURE2D_SAMPLER2D(_CameraDepthTexture, sampler_CameraDepthTexture);
    float4x4 _FrustumCornersRay;
    struct v2f
    {
        float4 vertex: SV_POSITION;
        float2 texcoord: TEXCOORD0;
        float2 texcoordStereo: TEXCOORD1;
        float4 interpolatedRay: TEXCOORD2;
    };
    
    struct vert_data
    {
        float3 vertex: POSITION;
    };
    
    v2f Vert(vert_data v)
    {
        v2f o;
        o.vertex = float4(v.vertex.xy, 0.0, 1.0);
        o.texcoord = TransformTriangleVertexToUV(v.vertex.xy);
        
        #if UNITY_UV_STARTS_AT_TOP
            o.texcoord = o.texcoord * float2(1.0, -1.0) + float2(0.0, 1.0);
        #endif
        
        o.texcoordStereo = TransformStereoScreenSpaceTex(o.texcoord, 1.0);
        int index = 0;
        float slice = 0.5;
        #if UNITY_UV_STARTS_AT_TOP
            if (o.texcoord.x < 0.5 && o.texcoord.y > 0.5)
            {
                o.interpolatedRay = _FrustumCornersRay[0];
            }
            else if(o.texcoord.x < 0.5 && o.texcoord.y < 0.5)
            {
                o.interpolatedRay = _FrustumCornersRay[1] * 2 - _FrustumCornersRay[0];
            }
            else
            {
                o.interpolatedRay = _FrustumCornersRay[2] * 2 - _FrustumCornersRay[0];
            }
        #else
            if(o.texcoord.x < 0.5 && o.texcoord.y < 0.5)
            {
                o.interpolatedRay = _FrustumCornersRay[1];
            }
            else if(o.texcoord.x > 0.5 && o.texcoord.y < 0.5)
            {
                o.interpolatedRay = _FrustumCornersRay[3] * 2 - _FrustumCornersRay[1];
            }
            else
            {
                o.interpolatedRay = _FrustumCornersRay[0] * 2 - _FrustumCornersRay[1];
            }
        #endif
        return o;
    }
    
    
    
    float4 Frag(v2f  i): SV_Target
    {
        float4 color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoord);
        
        
        float linearDepth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, i.texcoord));
        float3 worldPos = (_WorldSpaceCameraPos + linearDepth * i.interpolatedRay.xyz);
		float2 maskUV = float2((worldPos.x + 25) / 50, (worldPos.z + 25) / 50);
        float mask = SAMPLE_TEXTURE2D(_FogMask, sampler_FogMask, maskUV).r;
        if(maskUV.x < 0 || maskUV.x> 1 || maskUV.y > 1 || maskUV.x < 0) {
				mask = 1;
		}
        return color * mask;
    }
    
    ENDHLSL
    
    SubShader
    {
        Cull Off ZWrite Off ZTest Always
        
        Pass
        {
            HLSLPROGRAM
            
            #pragma vertex Vert
            #pragma fragment Frag
            
            ENDHLSL
            
        }
    }
}